console.log ("Hello 204!") ;

/*
	OBJECTS
		- is a data type that is used to represent real world object
		- it is a collection of related data and/or functionalities
		- Information is stored in object represented in 'key' : "value" pair

			key 	-> property of the object
			value 	-> actual data to be stored 
		
		Two ways of creating object in JS:

			1. Object literal Notation 
				-> let/const objectName = {"key" : "value"} ;

				SYNTAX:
					let/const = 
						{
						keyA : valueA,
						keyB : valueB,
						keyC : valueC ...	
						}
			
			2. Object Constructor Notation
				
				Object Instantiation 
				-> let object = new Object()
				
				SYNTAX:
					function ObjectName (keyA, keyB) 
						{
						this.propertyName = value;
						this.keyB = keyB;
						}

*/

let cellphone = 
	{
		name : "Nokia 3210" ,
		manufactureDate : 1999 ,

	} ;

console.log ("Result from object creation using literal notation:") ;
console.log (cellphone) ;
console.log (typeof cellphone) ;

let cellphone2 = 
	{
		name : "Motorola V3X" ,
		manufactureDate : 2000
	} ;

console.log (cellphone2) ; 

let phones = [cellphone, cellphone2]
console.log (phones) ;

// creating using Constructor function

function Laptop (name, manufactureDate)
	{
	this.nameProperty = name ;
	this.manufactureDateProperty = manufactureDate ;
	} ;
let laptop = new Laptop ("Lenovo" , 2008)
console.log (laptop) ;

let oldLaptop = Laptop ("IBM" , 1980)
console.log (oldLaptop) ; 				//undefined

let myLaptop = new Laptop ("Macbook Air" , 2020) ;
console.log (myLaptop) ;


// 	creating empty objects

let computer = {}
console.log (computer) ;

/*
	SIMILAR TO A NORMAL FUNCTION

let myComputer = new Object () ;
console.log (myComputer) ;

function student (name, age)
	{
		return (name)
	}
student ("miah") ;      			- calling a function only
let myName = student ("miah") ;		
									- storing the function in a var

*/

/*
	Accessing Object Properties
		1. Using dot notation
			SYNTAX:
			objectName.objectProperty

		2. Square bracket notation
			SYNTAX:
			objectName["objectProperty"]
*/

// using dot notation 
console.log (myLaptop.nameProperty) ;
console.log (myLaptop.manufactureDateProperty) ;

// using square bracket
console.log (myLaptop["nameProperty"]);


/*
	Accessing Array Objects
*/

let array = [laptop, myLaptop] ;
console.log (array) ;
// let array = [{name: "Lenovo" , manufactureDate: 2008} , {name: "Macbook" , manufactureDate: 2020}]

// using dot notation
console.log(array[1].nameProperty); 		//	Macbook Air

// using square bracket
console.log(array[0]["nameProperty"]);		// 	Lenovo


/*
	Initializing / Add / Delete / Reassigning Object Properties
*/

let car = {};
console.log (car) ;

/*
	1. Adding using dot notation
		SYNTAX:
		objectName.propertyName = value
*/
car.name = "Sarao" ;
console.log ("result from adding properties using dot notation") ;
console.log (car) ;

/*
	2. Adding using square bracket
		SYNTAX:
		objectName["propertyName"] = value
*/
car["manufactureDate"] = 2019 ;
console.log ("result from adding properties using square bracket") ;
console.log (car) ;

/*
	3. removing using delete
*/
delete car["manufactureDate"] ;
console.log (car) ;

car.manufactureDate = 2019 ;

/*
	4. reassigning object properties
		SYNTAX:
		objectName.propertyName = "New Value" ;
*/
car.name = "Mustang" ;
console.log (car) ;

/*
	OBJECT METHODS:
		- an object method is a function which is a propert if an object.
		- They are also functions and one of the key diferences they have is that methods are functions that is related to a specific object.
		- Methods are useful for creating object-specific functions which are used to perform tasks on them.
*/

let person = 
	{
		//property
		name : "Jack" ,

		//sample method
		talk : function	()
			{
				console.log ("Hello my name is " + this.name);
			} , 
	} ;

console.log (person);
// calling the function
person.talk();

person.walk = function()
	{
		console.log (this.name + " walked 25 steps forward.") ;
	}

person.walk ();
console.log (person) ;

/*
		- methods are useful for creating reusable functions that performs taks related to an object
*/

let friend = 
	{
		firstName : "Rafael" ,
		lastName : "Santillan" ,
		address: 
			{
				city : "Quezon City" ,
				country: "Philippines" ,
			} ,
		email: 
			[
			"raf@mail.com" ,
			"raf123@yahoo.com"
			] ,
		introduce: function()
			{
			console.log ("Hello! My name is " + this.firstName + " " + this.lastName ) ;	
			}
	}
friend.introduce () ;

// 	Real world application of objects

/*
	SCENARIO:

		1. We would like to create a game that would have several pokeon that will interact with each other
		2. Every pokemon would have the same set of stats, properties and functions

*/

let myPokemon = 
	{
		name: "Pikachu" ,
		level: 3 ,
		health: 100 ,
		attack: 50 ,

		tackle: function()
			{
				console.log ("This pokemon tackles the target pokemon");
				console.log ("target pokemon's health is now reduced to targetPokemonHealth")

			} ,

		faint: function()
			{
				console.log ("Pokemon fainted");
			}
	} 

console.log (myPokemon) ;

// using object constructor


function Pokemon (name , level , health)

//ORIGINAL ATTEMPT
	
	{
		this.name = name ;
		this.level = level ;
		this.health = health ;
		this.attack = (level*2)+20 ;

		//methods
		this.tackle = function (target)
			{
				console.log (this.name + " tackled " + target.name ) ;
				console.log (target.name + "'s health is now reduced to " + (target.health - this.attack)) ;
				
				target.health = target.health - this.attack ;

				this.faint = function ()
					{
						console.log (target.name + " has fainted") ;
					}

				if (target.health < 0)
					{
						this.faint () ;
					}
				
						
			} ;
		
	}


// INSTRUCTOR'S SOLUTION
/*

	{
		this.name = name ;
		this.level = level ;
		this.health = health ;
		this.attack = (level*2)+20 ;

		//methods
		this.tackle = function (target)
			{
				console.log (this.name + " tackled " + target.name ) ;
				console.log (target.name + "'s health is now reduced to " + (target.health - this.attack)) ;
				
				target.health -=  this.attack ;

				
				if (target.health < 0)
					{
					target.faint () ;
					}
				
						
			} ;

		this.faint = function ()
			{
				console.log (this.name + " has fainted") ;
			}
		
	}
*/


let squirtle = new Pokemon ("Squirtle" , 15 , 200 ) ;
let snorlax = new Pokemon ("Snorlax" , 75 , 500) ;
let rowlet = new Pokemon ("Rowlet" , 16 , 175) ;

console.log (squirtle , snorlax , rowlet) ;

// SYNTAX: 
// 	object.method(target) ;
	rowlet.tackle(squirtle) ;
	rowlet.tackle(squirtle) ;
	rowlet.tackle(squirtle) ;
	rowlet.tackle(squirtle) ;


